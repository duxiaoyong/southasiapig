D统计  BABA ABBA

150服务器： /home/xydu/00.soft/xyduABBAperl

核心算法来自： https://github.com/owensgl/abba_baba

perl脚本，可以计算每段区域的D fd，也可统计整个基因组或者染色体的D fd以及Z值。单个snp逐步计算，然后block，速度快，不占内存，计算的值很准。

  范例：
echo "Myanmar_Ayeyarwady China_Luchuan  Berkshire P_Africanus" > test1List

nohup sh        xyduABBAperl.sh         test1List       &

说明：

1） 参数为4个受测群体构成的文件，只能一行。（其实也可以改成多行，每行第一列为prefix  后面为 4 pops）

2 ）需要  indivPop 文件，格式与qDstat软件要求一样

3） 需要SNPtab.gz.list，注意这里我自己挖了个坑，list文件里面可以多行，每行含有一条染色体vcf转成SNPtab格式的文件名字。注意这个参数不是数据文件名，而是数据文件名的list。

4）SNPtab.gz 数据可以用 tped2SNPtab.pl 或者 vcf2SNPtab.pl 。 vcf(.gz) 建议每条染色体单独转化，然后把转化后的文件名按照chr1-chr18排序，ls *SNPtab.gz > SNPtab.gz.list

5） window size 的设置在ABBA_out_blocker.pl，默认 my $window_size = 200000;

6) 由一个入参$prefix，最后输出生成 文件$prefix.snpIndivDstat $prefix.averageDstat   $prefix.snpIndivDstat $prefix.snpIndivDstat.block $prefix.snpIndivDstat.block.jack  $prefix.WholeGenomeStatic 

 $prefix.snpIndivDstat :  每个snp的D值等统计，没很大意义

$prefix.averageDstat :  3列 AverageD AverageFhom AverageFd 

 $prefix.snpIndivDstat.block :  把每条染色体安装window size 统计 D Fd， 这个很合适分成染色体做曼哈顿图

 $prefix.snpIndivDstat.block.jack:  Jacknife 统计的p值

$prefix.WholeGenomeStatic :  9列数值， AverageD, 相应的 p值，Z 值,  AverageFhom  相应的 p值，Z 值, AverageFd, 相应的 p值，Z 值


 xyduABBAperl.sh

prefix=$1

softDir=/home/xydu/00.soft/xyduABBAperl

perl $softDir/ABBA_BABA.v2.dxy.pl SNPtab.gz.list indivPop $prefix $prefix.snpIndivDstat $prefix.averageDstat 

perl $softDir/ABBA_out_blocker.pl $prefix.snpIndivDstat > $prefix.snpIndivDstat.block 

Rscript $softDir/Jacknife_ABBA_pipe.R $prefix.snpIndivDstat.block $prefix.snpIndivDstat.block.jack

perl $softDir/get_D_Z.pl $prefix $prefix.averageDstat $prefix.snpIndivDstat.block.jack > $prefix.WholeGenomeStatic 

